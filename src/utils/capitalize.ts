export default function capitalize(str: String) {
  return str.charAt(0).toUpperCase().concat(str.substring(1, str.length));
}
