export const sortByAlphabeticallMoves = (list) => {
  list.sort(function (x, y) {
    let a = x.move.name.toUpperCase();
    let b = y.move.name.toUpperCase();

    return a == b ? 0 : a > b ? 1 : -1;
  });
  return list;
};

export const sortByAlphabeticallGames = (list) => {
  list.sort(function (x, y) {
    let a = x.version.name.toUpperCase();
    let b = y.version.name.toUpperCase();

    return a == b ? 0 : a > b ? 1 : -1;
  });
  return list;
};
