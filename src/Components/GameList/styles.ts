import { Grid, styled, Typography } from "@mui/material";

export const Title = styled(Typography)(({ theme }) => ({
  color: "#455a64",
  fontSize: 26,
  textAlign: "center",
  margin: theme.spacing(4, 0, 2, 0),
}));
