import { useState } from "react";
import { sortByAlphabeticallGames } from "@/utils/sort";
import { Button, Grid } from "@mui/material";
import { ItemList, TitleSection } from "@/components";
import { Title } from "./styles";
import theme from "@/styles/theme";

const GameList = ({ games }: any) => {
  const [open, setOpen] = useState(false);
  const gamesList = sortByAlphabeticallGames(games);
  const items = open ? -1 : 0;
  const handleOpenList = () => {
    setOpen(!open);
  };

  return (
    <div
      style={{
        overflowY: "scroll",
        height: open ? "461px" : 50,
        margin: theme.spacing(4, 0, 0, 0),
      }}
    >
      <TitleSection
        text="Games"
        small={games.length}
        open={open}
        action={setOpen}
      />
      <Grid container direction="column" component="ol">
        {gamesList
          .slice(0, items)
          .map((game: gameIndexProps, index: Number) => {
            return (
              <ItemList
                key={`${index}`}
                index={index}
                text={game.version.name}
              />
            );
          })}
      </Grid>
    </div>
  );
};

export default GameList;
