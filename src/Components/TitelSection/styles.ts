import { Grid, IconButton, styled, Typography } from "@mui/material";

export const WrapperTitle = styled("div")(({ theme }) => ({
  position: "sticky",
  top: 0,
  backgroundColor: "rgba(255, 255, 255, 0.6)",
  padding: theme.spacing(0, 0, 2, 0),
}));

export const Title = styled(Typography)(({ theme }) => ({
  color: "#455a64",
  fontSize: 24,
  // margin: theme.spacing(4, 0, 2, 0),
  "& small": {
    fontSize: 16,
  },
}));

export const ButtonPlus = styled(IconButton)(({ theme }) => ({
  position: "absolute",
  top: 0,
  right: 10,
}));
