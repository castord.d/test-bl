import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import { ButtonPlus, Title, WrapperTitle } from "./styles";

const TitleSection = ({ text, small, open, action }: TitleSectionsProps) => {
  return (
    <WrapperTitle>
      <Title>
        {text} <small>({parseInt(`${small}`) - 1})</small>
      </Title>
      <ButtonPlus
        title={`button - see ${open ? "less" : "more"}`}
        onClick={() => action(!open)}
      >
        {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      </ButtonPlus>
    </WrapperTitle>
  );
};

export default TitleSection;
