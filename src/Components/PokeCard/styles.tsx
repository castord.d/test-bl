import { ReactNode } from "react";
import { styled, Typography, Card, CardContent } from "@mui/material";
import { TypographyProps } from "@mui/material";

export const WrapperCard = styled(Card)(({ theme }) => ({
  width: "100%",
  minWidth: 345,
  maxWidth: 450,
  margin: theme.spacing(5, 0),
  padding: theme.spacing(0, 0, 4),
  borderRadius: "24px",
}));

export const ImageWrapper = styled("div")`
  position: relative;
  width: 150px;
  height: 150px;
  border-radius: 50%;
  border: 1px solid #ccc;
  margin: 0 auto;
  img {
    width: 100%;
    max-width: 150px;
  }
`;

export const Content = styled(CardContent)(({ theme }) => ({
  padding: theme.spacing(4),
}));

export const CardTitle = styled(Typography)(({ theme }) => ({
  width: "100%",
  color: "#455a64",
  fontSize: 36,
  textAlign: "center",
  margin: theme.spacing(2, 0),
}));

export const DescriptItem = styled(({ children }: TypographyProps) => (
  <Typography
    variant="subtitle2"
    component="span"
    style={{
      width: "100%",
      display: "block",
    }}
  >
    {children}
  </Typography>
))``;
