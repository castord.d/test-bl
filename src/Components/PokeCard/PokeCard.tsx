import Image from "next/image";
import capitalize from "@/utils/capitalize";
import { Grid } from "@mui/material";
import { MovesList, GameList } from "@/components";
import {
  WrapperCard,
  ImageWrapper,
  Content,
  CardTitle,
  DescriptItem,
} from "./styles";

const PokeCard = ({ data }: PokeCardProps) => {
  const {
    name,
    sprites,
    height,
    weight,
    base_experience,
    moves,
    game_indices,
  } = data;
  return (
    <WrapperCard elevation={6}>
      <Content>
        <ImageWrapper>
          <Image
            alt={`Image Pokemon ${name}`}
            src={`${sprites.other.dream_world.front_default}`}
            fill
            loading="lazy"
          />
        </ImageWrapper>
        <CardTitle variant="h1">{capitalize(name)}</CardTitle>
        <Grid
          container
          spacing={2}
          style={{ borderBottom: "1px solid #f1f1f1" }}
          p={2}
        >
          <Grid item xs={4} textAlign="center">
            <DescriptItem>Height</DescriptItem>
            <DescriptItem>{`${height ? height : "--"}`}</DescriptItem>
          </Grid>
          <Grid item xs={4} textAlign="center">
            <DescriptItem>Weight</DescriptItem>
            <DescriptItem>{`${weight ? weight : "--"}`}</DescriptItem>
          </Grid>
          <Grid item xs={4} textAlign="center">
            <DescriptItem>Experience</DescriptItem>
            <DescriptItem>
              {`${base_experience ? base_experience : "--"}`}
            </DescriptItem>
          </Grid>
        </Grid>
        <MovesList moves={moves} />
        <GameList games={game_indices} />
      </Content>
    </WrapperCard>
  );
};

export default PokeCard;
