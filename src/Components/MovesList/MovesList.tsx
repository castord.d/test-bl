import { Grid } from "@mui/material";

import { sortByAlphabeticallMoves } from "@/utils/sort";
import { useState } from "react";
import { ItemList, TitleSection } from "@/components";
import theme from "@/styles/theme";

const MovesList = ({ moves }: any) => {
  const [open, setOpen] = useState(false);
  const items = open ? -1 : 0;
  const movesList = sortByAlphabeticallMoves(moves);

  return (
    <div
      style={{
        overflowY: "scroll",
        height: open ? "435px" : 50,
        margin: theme.spacing(4, 0, 0, 0),
      }}
    >
      <TitleSection
        text="Moves"
        small={moves.length}
        open={open}
        action={setOpen}
      />
      <Grid container direction="column">
        {movesList.slice(0, items).map((mov: movesProps, index: Number) => {
          return (
            <ItemList key={`${index}`} text={mov.move.name} index={index} />
          );
        })}
      </Grid>
    </div>
  );
};

export default MovesList;
