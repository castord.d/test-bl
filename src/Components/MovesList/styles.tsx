import { Grid, IconButton, styled, Typography } from "@mui/material";

export const Title = styled(Typography)(({ theme }) => ({
  color: "#455a64",
  fontSize: 24,
  margin: theme.spacing(4, 0, 2, 0),
  "& small": {
    fontSize: 16,
  },
}));

export const ButtonPlus = styled(IconButton)(({ theme }) => ({
  position: "absolute",
  top: 0,
  right: 10,
}));
