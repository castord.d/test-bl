import { Typography } from "@mui/material";
import HeartBrokenIcon from "@mui/icons-material/HeartBroken";

const ErrorCard = () => {
  return (
    <>
      <HeartBrokenIcon fontSize="large" color="error" />
      <Typography variant="h5" align="center">
        Something Unexpected Happened
      </Typography>
    </>
  );
};

export default ErrorCard;
