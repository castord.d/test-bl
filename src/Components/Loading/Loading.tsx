import { CircularProgress, Typography } from "@mui/material";

const Loading = () => {
  return (
    <>
      <CircularProgress />
      <Typography variant="subtitle1" component="span">
        Loading...
      </Typography>
    </>
  );
};

export default Loading;
