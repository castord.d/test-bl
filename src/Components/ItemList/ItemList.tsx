import { Item } from "./styles";

const ItemList = ({ text, index }: ItemListProps) => {
  return (
    <Item item>
      <span>{parseInt(`${index}`) + 1}</span>
      <span>{`${text}`}</span>
    </Item>
  );
};

export default ItemList;
