import { Grid, styled } from "@mui/material";

export const Item = styled(Grid)(({ theme }) => ({
  display: "flex",
  fontSize: "0.8rem",
  textAlign: "center",
  padding: theme.spacing(1, 2),
  borderTop: "1px solid #f1f1f1",
  transition: "all ease-in-out 300ms",
  justifyContent: "space-between",
}));
