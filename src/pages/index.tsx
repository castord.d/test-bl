import Head from "next/head";
import useAxios from "axios-hooks";

import { Container, Grid } from "@mui/material";
import { PokeCard, Loading, ErrorCard } from "@/components";

export default function Home() {
  const [{ data, loading, error }] = useAxios(
    "https://pokeapi.co/api/v2/pokemon/charmander/"
  );

  return (
    <>
      <Head>
        <title>Pokedex | Bloomber Línea</title>
        <meta
          name="description"
          content="Pokedex for testing develop abilities"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          style={{ minHeight: "calc(100vh)" }}
        >
          {loading && <Loading />}
          {error && <ErrorCard />}
          {data && !error && <PokeCard data={data} />}
        </Grid>
      </Container>
    </>
  );
}
