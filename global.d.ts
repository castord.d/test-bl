declare interface abilityProps {
  ability: {
    name: String;
    url: String;
  };
}

declare interface versionGroupDetailsProps {
  level_learned_at: Number;
  move_learn_method: {
    name: String;
    url: String;
  };
  version_group: {
    name: String;
    url: String;
  };
}

declare interface movesProps {
  move: {
    name: String;
    url: String;
  };
  version_group_details: versionGroupDetailsProps[];
}

declare interface gameIndexProps {
  game_index: Number;
  version: {
    name: String;
    url: String;
  };
}

declare interface spritesProps {
  back_default: String;
  back_female?: String;
  back_shiny: String;
  back_shiny_female?: String;
  front_default: String;
  front_female?: String;
  front_shiny: String;
  front_shiny_female?: String;
  other: {
    dream_world: {
      front_default: String;
      front_female?: String;
    };
    home: {
      front_default: String;
      front_female?: String;
      front_shiny: String;
      front_shiny_female?: String;
    };
    "official-artwork": {
      front_default: String;
      front_shiny: String;
    };
  };
  versions: {
    "generation-i": {
      "red-blue": {
        back_default: String;
        back_gray: String;
        back_transparent: String;
        front_default: String;
        front_gray: String;
        front_transparent: String;
      };
      yellow: {
        back_default: String;
        back_gray: String;
        back_transparent: String;
        front_default: String;
        front_gray: String;
        front_transparent: String;
      };
    };
    "generation-ii": Object;
    "generation-iii": Object;
    "generation-iv": Object;
    "generation-v": Object;
    "generation-vi": Object;
    "generation-vii": Object;
    "generation-viii": Object;
  };
}

declare interface statProps {
  base_stat: Number;
  effort: Number;
  stat: {
    name: String;
    url: String;
  };
}

declare interface typesProps {
  slot: Number;
  type: {
    name: String;
    url: String;
  };
}

declare interface PokeCardProps {
  data: {
    abilities: abilityProps[];
    base_experience: Number;
    forms: {
      name: String;
      url: String;
    };
    game_indices: gameIndexProps[];
    height: Number;
    held_items: Array;
    id: Number;
    is_default: Boolean;
    location_area_encounters: String;
    moves: movesProps[];
    name: String;
    order: Number;
    past_types: Array;
    species: {
      name: String;
      url: String;
    };
    sprites: spritesProps;
    stats: statProps[];
    types: typesProps[];
    weight: Number;
  };
}

declare interface ItemListProps {
  text: String;
  index: Number;
}

declare interface TitleSectionsProps {
  text: String;
  small: String;
  open: Boolean;
  action: Function;
}
